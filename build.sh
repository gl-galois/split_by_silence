#!/bin/sh

# Settings
PYTHON_FILES="main.py command.py audio_processor.py util.py"

DIST_DIR="sbs.d"
COMMAND_NAME="sbs"
RELEASE_DIR="release.d"

# Working Directory
SCRIPT_DIR=$(cd $(dirname $0); pwd)
echo "Working Directory: ${SCRIPT_DIR}"

# Build
pyinstaller -y --clean --name ${COMMAND_NAME} --distpath ${DIST_DIR} \
  --exclude-module _tkinter --exclude-module Tkinter --exclude-module enchant --exclude-module twisted \
  ${PYTHON_FILES}

# Bundle support files
cp config.json                 ${DIST_DIR}/
cp build_includes.d/README.md  ${DIST_DIR}/
cp build_includes.d/sbs.sh     ${DIST_DIR}/
cp split_by_silence_sample.mp3 ${DIST_DIR}/

zip -r ${RELEASE_DIR}/${DIST_DIR}.zip ${DIST_DIR}

echo "Release: ${RELEASE_DIR}/${DIST_DIR}.zip"