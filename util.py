# debug mode
# usage: $ python -O script.py
DEBUG_FLAG       = not __debug__
def debug(*args, **kwargs):
    if DEBUG_FLAG:
        print(*args, **kwargs)
