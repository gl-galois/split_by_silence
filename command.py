import os, sys, re

from prompt_toolkit import prompt
from prompt_toolkit.history import FileHistory
from prompt_toolkit.auto_suggest import AutoSuggestFromHistory
#from prompt_toolkit.contrib.completers import WordCompleter
from prompt_toolkit.completion import WordCompleter

class CommandLine:

    def __init__(self, options, audio_processor):
        self.verbose_mode  = options.verbose_mode
        self.proc          = audio_processor
        self.HISTORY_PATH  = os.environ["HOME"] + "/.sbs_history"
        self.SBS_COMPLETER = WordCompleter([
            'sound_file=',
            'silence_length=',
            'silence_thresh=',
            'additional_silence=',
            'normal_silence=',
            'verbose_mode=',
            'print-commands-help',
            'print-settings',
            'list-loaded-chunks',
            'list-new-chunks',
            'load-and-split',
            'save-new-sound',
            'save-loaded-chunks',
            'play-loaded-chunks',
            'play-new-chunks',
            'play',
            'play-next',
            'play-new',
            'push-to-new',
            'push-to-new-range',
            'push-to-new-all',
            'append-to-new',
            'merge-new-range',
            'delete-new-range',
            'refresh-new',
            'recognize-text',
        ],ignore_case=False)


    # ========
    # Commands
    # ========

    # show commands
    def print_commands_help(self):
        print("=== [print commands help] ===")
        print("""
        Settings:

          sound_file         = "file.mp3" # sound file to split
          silence_length     = 100        # specify length of silence MILLISEC to split sound
          silence_thresh     = -80        # specify threshold dBFS to consider sound as silence to split sound
          additional_silence = 1.0        # specify additional SEC of silence duration for pause file
          normal_silence     = 0.5        # specify SEC of silence duration for normal file
          verbose_mode       = True       # execute program with messages


        Commands:

          show commands:
            print-commands-help          # TODO doc
            print-settings               # TODO doc
            list-loaded-chunks           # TODO doc
            list-new-chunks              # TODO doc

          load / save commands:  
            load-and-split               # TODO doc
            save-new-sound [file]        # TODO doc
            save-loaded-chunks           # TODO doc

          play commands:  
            play-loaded-chunks           # TODO doc
            play-new-chunks              # TODO doc
            play [play_index]            # TODO doc
            play-next                    # TODO doc
            play-new [play_index]        # TODO doc

          edit commands:  
            push-to-new [new_index]      # TODO doc
            push-to-new-range start end  # TODO doc
            push-to-new-all              # TODO doc
            append-to-new [new_index]    # TODO doc
            merge-new-range start end    # TODO doc
            delete-new-range start [end] # TODO doc
            refresh-new                  # TODO doc
            recognize-text               # TODO doc
        """)

    def print_settings(self):
        print("=== [print settings] ===")
        print("sound_file \t\t=",       '"'+self.proc.sound_file+'"')
        print("silence_length \t\t=",   self.proc.silence_length)
        print("silence_thresh \t\t=",   self.proc.silence_thresh)
        print("additional_silence \t=", self.proc.additional_silence)
        print("normal_silence \t\t=",   self.proc.normal_silence)
        print("verbose_mode \t\t=",     self.verbose_mode)

    def list_loaded_chunks(self):
        print("=== [print loaded_chunks] ===")
        self.list_chunks(self.proc.loaded_chunks)

    def list_new_chunks(self):
        print("=== [print new_chunks] ===")
        self.list_chunks(self.proc.new_chunks)

    # load and save commands
    def load_and_split(self):
        if self.verbose_mode:
            print("=== [loading mp3 file:", self.proc.sound_file, "] ===")
        self.proc.split_audiofile()
        if self.verbose_mode:
            self.list_chunks(self.proc.loaded_chunks)

    def save_new_sound(self, file=None):
        pause_file, normal_file = self.proc.save_audiofile(file)
        if self.verbose_mode:
            print("=== exporting pause file:",  pause_file)
            print("=== exporting normal file:", normal_file)

    def save_loaded_chunks(self):
        self.proc.save_loaded_chunks_audiofiles()

    # play commands
    def play_loaded_chunks(self):
        if self.verbose_mode:
            print("=== [play loaded_chunks] ===")
        self.play_chunks(self.proc.loaded_chunks)

    def play_new_chunks(self):
        if self.verbose_mode:
            print("=== [play new_chunks] ===")
        self.play_chunks(self.proc.new_chunks)

    def play(self, play_index=None):
        index, chunk = self.proc.loaded_chunk(play_index)
        if self.verbose_mode:
            print("=== play index:", index)
            if 'text' in chunk:
                print("=== audio text:", chunk["text"])
        self.proc.play(play_index)

    def play_next(self):
        index, chunk = self.proc.loaded_chunk_next()
        if self.verbose_mode:
            print("=== play index:", index)
            if 'text' in chunk:
                print("=== audio text:", chunk["text"])
        self.proc.play_next()

    def play_new(self, play_index=None):
        index, chunk = self.proc.new_chunk(play_index)
        if self.verbose_mode:
            print("=== play index:", index)
            if 'text' in chunk:
                print("=== audio text:", chunk["text"])
        self.proc.play_new(play_index)

    # edit commands
    def push_to_new(self, index=None):
        index, chunk = self.proc.push_chunk_loaded_to_new(index)
        if self.verbose_mode:
            print("=== pushed. index:", index, "to end of new chunks, duration:", chunk["duration"])

    def push_to_new_range(self, start_index, end_index):
        for index in range(start_index, end_index + 1):
            self.push_to_new(index)

    def push_to_new_all(self):
        self.proc.push_all_chunks_loaded_to_new()
        print("=== pushed all.")

    def append_to_new(self, new_index=None):
        new_chunk = self.proc.append_chunk_loaded_to_new(new_index)
        print("=== appended. new index:", new_index, ", duration:", new_chunk["duration"])

    def merge_new_range(self, start_index, end_index):
        new_chunk = self.proc.merge_new_chunks_range(start_index, end_index)
        print("=== merged. new from index:", start_index, " to index:", end_index, ", duration:", new_chunk["duration"])

    def delete_new_range(self, start, end=None):
        pass

    def refresh_new(self):
        self.proc.refresh_new_chunks()
        if self.verbose_mode:
            print("new list refreshed.")

    def recognize_text(self):
        for index, chunk in self.proc.chunks_to_text(self.proc.loaded_chunks):
            # recognize chunk's audio and get text in iterator chunks_to_text()
            if self.verbose_mode:
                print("index:", index, "text:", chunk['text'])

    # =================
    # Utility Functions
    # =================
    def list_chunks(self, chunks):
        cumulative_time = 0.0
        for index, chunk in enumerate(chunks):
            cumulative_time += chunk["duration"]
            self.print_chunk(index, chunk, cumulative_time)

    def print_chunk(self, index, chunk, cumulative_time=None):
        if 'text' in chunk:
            print("index: {0:d}\tduration: {1:06.2f}\tcumulative time: {2:08.2f}\ttext: {3}".format(
                index, chunk["duration"], cumulative_time, chunk["text"]))
        else:
            print("index: {0:d}\tduration: {1:06.2f}\tcumulative time: {2:08.2f}".format(
                index, chunk["duration"], cumulative_time))

    def play_chunks(self, chunks):
        cumulative_time = 0.0
        for index, chunk in self.proc.play_chunks(chunks):
            # play chunk's audio in iterator play_chunks()
            if self.verbose_mode:
                cumulative_time += chunk["duration"]
                self.print_chunk(index, chunk, cumulative_time)

    def repl(self):
        while 1:
            user_input_str = prompt('SBS> ',
                                    history=FileHistory(self.HISTORY_PATH),
                                    auto_suggest=AutoSuggestFromHistory(),
                                    completer=self.SBS_COMPLETER
                                    )
            user_inputs = user_input_str.strip().split(" ")
            command = user_inputs[0]
            if re.match('print-commands-help$',  command):
                self.print_commands_help()

            elif re.match('print-settings$',     command):
                self.print_settings()

            elif re.match('list-loaded-chunks$', command):
                self.list_loaded_chunks()

            elif re.match('list-new-chunks$',    command):
                self.list_new_chunks()

            elif re.match('load-and-split$',     command):
                self.load_and_split()

            elif re.match('save-new-sound$',     command):
                if len(user_inputs) > 1:
                    self.save_new_sound(user_inputs[1])
                else:
                    self.save_new_sound()

            elif re.match('save-loaded-chunks$', command):
                self.save_loaded_chunks()

            elif re.match('play-loaded-chunks$', command):
                self.play_loaded_chunks()

            elif re.match('play-new-chunks$',    command):
                self.play_new_chunks()

            elif re.match('play$',               command):
                if len(user_inputs) > 1:
                    self.play(int(user_inputs[1]))
                else:
                    self.play()

            elif re.match('play-next$',          command):
                self.play_next()

            elif re.match('play-new$',           command):
                if len(user_inputs) > 1:
                    self.play_new(int(user_inputs[1]))
                else:
                    self.play_new()

            elif re.match('push-to-new$',        command):
                if len(user_inputs) > 1:
                    self.push_to_new(int(user_inputs[1]))
                else:
                    self.push_to_new()

            elif re.match('push-to-new-range$',  command):
                self.push_to_new_range(int(user_inputs[1]), int(user_inputs[2]))

            elif re.match('push-to-new-all$',    command):
                self.push_to_new_all()

            elif re.match('append-to-new$',      command):
                if len(user_inputs) > 1:
                    self.append_to_new(int(user_inputs[1]))
                else:
                    self.append_to_new()

            elif re.match('merge-new-range$',    command):
                self.merge_new_range(int(user_inputs[1]), int(user_inputs[2]))

            elif re.match('delete-new-range$',   command):
                if len(user_inputs) > 2:
                    self.delete_new_range(int(user_inputs[1]), int(user_inputs[2]))
                else:
                    self.delete_new_range(int(user_inputs[1]))

            elif re.match('refresh-new$',        command):
                self.refresh_new()

            elif re.match('recognize-text$',     command):
                self.recognize_text()

            elif re.match('exit$',               command):
                sys.exit(0)

            elif re.match('sound_file\s*=',         user_input_str):
                exec("self.proc." + user_input_str, globals(), locals())

            elif re.match('silence_length\s*=',     user_input_str):
                exec("self.proc." + user_input_str, globals(), locals())

            elif re.match('silence_thresh\s*=',     user_input_str):
                exec("self.proc." + user_input_str, globals(), locals())

            elif re.match('additional_silence\s*=', user_input_str):
                exec("self.proc." + user_input_str, globals(), locals())

            elif re.match('normal_silence\s*=',     user_input_str):
                exec("self.proc." + user_input_str, globals(), locals())

            elif re.match('verbose_mode\s*=',       user_input_str):
                exec("self." + user_input_str,      globals(), locals())

            elif re.match('!',                      user_input_str):
                # execute python code
                exec(user_input_str,                globals(), locals())

            else:
                print("no such command:", user_input_str)

