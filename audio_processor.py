import os, copy

from pydub import AudioSegment
from pydub.silence import split_on_silence
from pydub.playback import play as sound_play

import speech_recognition as sr
from tempfile import NamedTemporaryFile

from util import *

SR_FMT="wav"

class AudioProcessor:

    def __init__(self, config, options):
        # config file parameters
        self.out_dir               = config["output_directory"]
        self.new_fmt               = config["new_audio_file_format"]
        self.head_silence_duration = config["head_silence_duration"]
        self.pause_file_preffix    = config["pause_file_prefix"]
        self.normal_file_preffix   = config["normal_file_prefix"]

        # processor parameters
        self.sound_file         = options.sound_file
        self.silence_length     = options.silence_length
        self.silence_thresh     = options.silence_thresh
        self.additional_silence = options.additional_silence
        self.normal_silence     = options.normal_silence

        # variables
        self.loaded_chunks = []  # [ {voice: <AS>, silence: <AS>, duration: <num>}, {}... ] where AS is AudioSegment
        self.new_chunks    = []  # same as above structure
        self.loaded_index  = 0
        self.recognizer    = sr.Recognizer()

    # ====================
    # Load and Split Audio
    # ====================
    def split_audiofile(self):
        loaded_sound = AudioSegment.from_mp3(self.sound_file)
        chunks = split_on_silence(loaded_sound,
            # must be silent for at least half a second
            min_silence_len=self.silence_length,  # 100
            # consider it silent if quieter than -16 dBFS
            silence_thresh=self.silence_thresh  # -50
        )

        self.refresh_loaded()
        for index, c in enumerate(chunks):
            chunk = {
                "voice": c,
                "silence": AudioSegment.silent(duration=c.duration_seconds * 1000 + int(self.additional_silence * 1000)),
                "duration": c.duration_seconds
            }
            self.loaded_chunks.append(chunk)

    def refresh_loaded(self):
        self.loaded_chunks = []
        self.loaded_index = 0

    def save_audiofile(self, file=None):
        if file is None:
            sound_path_base, _ = os.path.splitext(self.sound_file)
        else:
            sound_path_base = os.path.join(os.path.dirname(self.sound_file), file)

        try:
            dirpath  = os.path.dirname(sound_path_base)
            basename = os.path.basename(sound_path_base)
            out_file_pause  = os.path.join(self.out_dir, dirpath, self.pause_file_preffix  + basename + "." + self.new_fmt)
            out_file_normal = os.path.join(self.out_dir, dirpath, self.normal_file_preffix + basename + "." + self.new_fmt)
            joined_chunk_pause  = self.merge_all_new_sound(pause=True)
            joined_chunk_normal = self.merge_all_new_sound(pause=False)

            os.makedirs(os.path.join(self.out_dir, dirpath), exist_ok=True)
            joined_chunk_pause.export(out_file_pause,   format=self.new_fmt)
            joined_chunk_normal.export(out_file_normal, format=self.new_fmt)
        except FileNotFoundError as e:
            print("cannot save file because :")
            print('type:' + str(type(e)))
            print('args:' + str(e.args))
            print('e:' + str(e))
            return (None, None)

        return out_file_pause, out_file_normal

    def save_loaded_chunks_audiofiles(self):
        sound_path_base, _ = os.path.splitext(self.sound_file)
        dirpath  = os.path.dirname(sound_path_base)
        basename = os.path.basename(sound_path_base)

        out_dirpath = os.path.join(self.out_dir, dirpath, 'devide') # TODO: constantify
        os.makedirs(out_dirpath, exist_ok=True)
        try:
            for idx in range(len(self.loaded_chunks)):
                out_file = os.path.join(out_dirpath, basename + ("_%02d" % idx) + "." + self.new_fmt)
                self.loaded_chunks[idx]['voice'].export(out_file, format=self.new_fmt)
        except FileNotFoundError as e:
            print("cannot save file because :")
            print('type:' + str(type(e)))
            print('args:' + str(e.args))
            print('e:' + str(e))

    def merge_all_new_sound(self, pause=True):
        joined_chunk         = AudioSegment.silent(duration=self.head_silence_duration * 1000)
        normal_silence_chunk = AudioSegment.silent(duration=int(self.normal_silence * 1000))
        for index, chunk in enumerate(self.new_chunks):
            if pause:
                joined_chunk = joined_chunk.append(chunk["voice"], crossfade=0).append(chunk["silence"],     crossfade=0)
            else:
                joined_chunk = joined_chunk.append(chunk["voice"], crossfade=0).append(normal_silence_chunk, crossfade=0)
        return joined_chunk

    # ===============
    # Get Audio Chunk
    # ===============
    def loaded_chunk(self, index=None):
        if index is None:
            index = self.loaded_index
        chunk = self.loaded_chunks[index]
        return index, chunk

    def loaded_chunk_next(self):
        index = self.loaded_index + 1
        return self.loaded_chunk(index)

    def new_chunk(self, index=None):
        if index is None:
            index = len(self.new_chunks) - 1
        chunk = self.new_chunks[index]
        return index, chunk

    # ==========
    # Play Audio
    # ==========
    def play_chunks(self, chunks):
        for index, chunk in enumerate(chunks):
            yield index, chunk
            sound_play(chunk["voice"])

    def play(self, play_index=None):
        if play_index is None:
            play_index = self.loaded_index
        self.play_chunk_at(self.loaded_chunks, play_index)

    def play_chunk_at(self, chunks, index):
        sound_play(chunks[index]["voice"])

    def play_next(self):
        self.loaded_index += 1
        self.play()

    def play_new(self, play_index=None):
        if play_index is None:
            play_index = len(self.new_chunks) - 1
        self.play_chunk_at(self.new_chunks, play_index)

    # ==========
    # Edit Audio
    # ==========
    def push_chunk_loaded_to_new(self, index=None):
        if index is None:
            index = self.loaded_index
        copy_chunk = copy.deepcopy(self.loaded_chunks[index])
        self.new_chunks.append(copy_chunk)
        return index, copy_chunk

    def push_all_chunks_loaded_to_new(self):
        for chunk in self.loaded_chunks:
            copy_chunk = copy.deepcopy(chunk)
            self.new_chunks.append(copy_chunk)

    def append_chunk_loaded_to_new(self, new_index=None):
        if new_index is None:
            new_index = len(self.new_chunks) - 1
        self.new_chunks[new_index]["voice"]    = self.new_chunks[new_index]["voice"].append(  self.loaded_chunks[self.loaded_index]["voice"],   crossfade=0)
        self.new_chunks[new_index]["silence"]  = self.new_chunks[new_index]["silence"].append(self.loaded_chunks[self.loaded_index]["silence"], crossfade=0)
        self.new_chunks[new_index]["duration"] = self.new_chunks[new_index]["duration"] +     self.loaded_chunks[self.loaded_index]["duration"]
        return self.new_chunks[new_index]

    def merge_new_chunks_range(self, start_index, end_index):

        chunk_voice    = AudioSegment.silent()
        chunk_silence  = AudioSegment.silent()
        chunk_duration = 0.0
        chunk_text     = ""
        for chunk in self.new_chunks[start_index: end_index + 1]:
            chunk_voice    = chunk_voice.append(chunk["voice"],     crossfade=0)
            chunk_silence  = chunk_silence.append(chunk["silence"], crossfade=0)
            chunk_duration = chunk_duration + chunk["duration"]
            if "text" in chunk:
                chunk_text = chunk_text + " " + chunk["text"]
        chunk_text = chunk_text.strip()
        merged_chunk = {
            "voice"    : chunk_voice,
            "silence"  : chunk_silence,
            "duration" : chunk_duration,
            "text"     : chunk_text
        }
        self.new_chunks[start_index: end_index + 1] = [merged_chunk]
        return merged_chunk

    def refresh_new_chunks(self):
        self.new_chunks = []

    # =============
    # Audio to Text
    # =============
    def chunks_to_text(self, chunks):
        for index, chunk in enumerate(chunks):
            if 'text' not in chunk:
                audio_segment = chunk['voice']
                text = self.voice_to_text(audio_segment)
                chunk['text'] = text
            yield index, chunk

    def voice_to_text(self, audio_segment):
        # change audio_segment format into sr(speech_recognition) format
        sr_audio = self.create_sr_audio(audio_segment)
        return self.sr_audio_to_text(sr_audio)

    def create_sr_audio(self, audio_segment):
        tempfile = NamedTemporaryFile(mode="rb+", delete=False)
        #audio_segment.export(tempfile, format=self.new_fmt)
        audio_segment.export(tempfile, format=SR_FMT)
        with sr.AudioFile(tempfile.name) as source:
            sr_audio = self.recognizer.record(source)

        return sr_audio

    def sr_audio_to_text(self, sr_audio):
        try:
            audio_text = self.recognizer.recognize_google(sr_audio)
            debug("text:", audio_text)
        except:
            audio_text = ""
            debug("can't convert audio to text.")

        return audio_text
