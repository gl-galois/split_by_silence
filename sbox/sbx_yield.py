def foos():
    yield("foo1", "bar1")
    yield("foo2")
    yield("foo3")

for f in foos():
    print("f", f)