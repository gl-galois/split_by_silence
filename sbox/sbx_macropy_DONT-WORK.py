from macropy.case_classes import macros, case

@case
class Point(x, y): pass

p = Point(1, 2)
print(p.x)

print(p)
Point(1, 2)
