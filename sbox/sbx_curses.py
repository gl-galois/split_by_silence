import curses
from time import sleep

def main(stdscr, rng=11):
    stdscr.clear()
    for i in range(0, rng):
        v = i-10
        stdscr.addstr(i, 0, '10 divided by {} is {}'.format(v, 10/v))
    stdscr.refresh()

    sleep(3)
    pad = curses.newpad(100, 100)
    for y in range(0, 100):
        for x in range(0, 100):
            try:
                pad.addch(y, x, ord('a') + (x * x + y * y) % 26)
            except curses.error:
                pass
    pad.refresh(0, 0, 5, 5, 20, 75)
    sleep(3)
    pad.clear()
    pad.refresh(0, 0, 5, 5, 20, 75)

    stdscr.getkey()

#curses.wrapper(main)
curses.wrapper(main, 5)

