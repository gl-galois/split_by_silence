import speech_recognition as sr

from pydub import AudioSegment
from pydub.silence import split_on_silence
from pydub.playback import play as sound_play


r = sr.Recognizer()
m = sr.Microphone()

print("A moment of silence, please...")
with m as source: r.adjust_for_ambient_noise(source)
print("Set minimum energy threshold to {}".format(r.energy_threshold))

def get_speech():
    global r, m, audio
    print("Say something!")

    with m as source:
        audio = r.listen(source)
    print("audio:", audio)
    wav_file = audio.get_wav_data()
    seg = AudioSegment.from_wav(wav_file)
    print("seg:", seg)
    print("Got it! Now to recognize it...")
    sound_play(seg)
    try:
        # recognize speech using Google Speech Recognition
        value = r.recognize_google(audio)

        # we need some special handling here to correctly print unicode characters to standard output
        if str is bytes:  # this version of Python uses bytes for strings (Python 2)
            print(u"You said {}".format(value).encode("utf-8"))
        else:  # this version of Python uses unicode for strings (Python 3+)
            print("You said {}".format(value))
    except sr.UnknownValueError:
        print("Oops! Didn't catch that")
    except sr.RequestError as e:
        print("Uh oh! Couldn't request results from Google Speech Recognition service; {0}".format(e))

