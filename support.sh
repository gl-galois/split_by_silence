alias vlplay="cvlc --play-and-exit"
alias sbs="python main.py -v"
alias sbs_opinion="python main.py -v -l 200 -t -40"
alias sbs_grammar-bdv="python main.py -bdv -l 1100 -t -40"
alias sbs_grammar-dv="python main.py -dv -l 1100 -t -40"
