Split By Silence
=
## abstract

TODO

## prerequisite

```zsh
% python -V
Python 3.8.6

% which ffmpeg
/usr/local/bin/ffmpeg

# If you don't have ffmpeg, install it.
% brew install ffmpeg
```

## installation

```zsh
% pip install -r requirements.txt
```

## usage

TODO

## configuration (config.json)

```zsh
  # constant values
  "output_directory"            : # TODO doc, ex) "./audio_for_repeat/",
  "new_audio_file_format"       : # TODO doc, ex) "mp3",
  "command_parser_format_width" : # TODO doc, ex) 180,
  "head_silence_duration"       : # TODO doc, ex) 1,
  "pause_file_prefix"           : # TODO doc, ex) "Pause_",
  "normal_file_prefix"          : # TODO doc, ex) "Normal_",

  # default values of global variables
  "default_batch_mode"         : # TODO doc, ex) false,
  "default_sound_file"         : # TODO doc, ex) "split_by_silence_sample.mp3",
  "default_silence_length"     : # TODO doc, ex) 100, # [millisec]",
  "default_silence_thresh"     : # TODO doc, ex) -80, # [dDFS]",
  "default_additional_silence" : # TODO doc, ex) 1.0, # [sec]",
  "default_normal_silence"     : # TODO doc, ex) 0.5, "# [sec]",
  "default_verbose_mode"       : # TODO doc, ex) false
```
 
## alias

```zsh
% source ./support.sh
% alias
sbs='python main.py -v'
sbs_grammar-bdv='python main.py -bdv -l 1100 -t -40'
sbs_grammar-dv='python main.py -dv -l 1100 -t -40'
sbs_opinion='python main.py -v -l 200 -t -40'
vlplay='cvlc --play-and-exit'
```

## build by pyinstaller

```zsh
% ./build.sh

or manually

% pyinstaller --name sbs -r config.json main.py command.py audio_processor.py util.py
% pyinstaller -y --clean --windowed --name sbs --exclude-module _tkinter --exclude-module Tkinter --exclude-module enchant --exclude-module twisted -r config.json main.py command.py audio_processor.py util.py
% pyinstaller -y --clean --name sbs --distpath sbs.d \
  --exclude-module _tkinter --exclude-module Tkinter --exclude-module enchant --exclude-module twisted \
  main.py command.py audio_processor.py util.py

```

## program files

* main.py : includes the program entrypoint.
* command.py : defines commands that can be used in "SBS> ".
* audio_processor.py : defines functions that manipulate audio.
* util.py : utilities.

## todo
* changing play() method to play silence without verbose mode.
* changing all methods to print audio-text only in verbose mode.
* changing show format of list commands like below

|index|duration|cumulative|text|
|----:|-------:|---------:|:---|
|    0|  0001.1|    0001.1|foo bar baz|
|    1|  0001.1|    0002.2|hoge fuga |

* implement delete_new_range() method
* create output_directory(defined in config.json) if not exist.

