import sys
from optparse import OptionParser
import json

from util import *
from command import *
from audio_processor import *

def process_options(config):

    parser = OptionParser()
    parser.formatter.width = config["command_parser_format_width"]

    parser.add_option("-b", "--batch",   dest="batch_mode",         default=config["default_batch_mode"],         action="store_true",              help="execute program with batch mode [default: "+str(config["default_batch_mode"])+"]")
    parser.add_option("-d", "--devide",  dest="devide_mode",        default=config["default_devide_mode"],        action="store_true",              help="execute program with devide mode [default: "+str(config["default_devide_mode"])+"]")
    parser.add_option("-f", "--file",    dest="sound_file",         default=config["default_sound_file"],         metavar="INPUT_FILE",             help="load INPUT_FILE and split it [default: "+config["default_sound_file"]+"]")
    parser.add_option("-l", "--length",  dest="silence_length",     default=config["default_silence_length"],     metavar="MILLISEC", type="int",   help="specify length of silence MILLISEC to split sound [default: "+str(config["default_silence_length"])+"]")
    parser.add_option("-s", "--silence", dest="additional_silence", default=config["default_additional_silence"], metavar="SEC",      type="float", help="specify additional SEC of silence duration for pause file [default: "+str(config["default_additional_silence"])+"]")
    parser.add_option("-n", "--normsil", dest="normal_silence",     default=config["default_normal_silence"],     metavar="SEC",      type="float", help="specify SEC of silence duration for normal file [default: " + str(config["default_normal_silence"]) + "]")
    parser.add_option("-t", "--thresh",  dest="silence_thresh",     default=config["default_silence_thresh"],     metavar="dBFS",     type="int",   help="specify threshold dBFS to consider sound as silence to split sound [default: "+str(config["default_silence_thresh"])+"]")
    parser.add_option("-v", "--verbose", dest="verbose_mode",       default=config["default_verbose_mode"],       action="store_true",              help="execute program without messages [default: " + str(config["default_verbose_mode"]) + "]")

    (options, args) = parser.parse_args()
    debug("options:", options)
    debug("args:", args)

    return options

if __name__ == "__main__":
    # load configuration
    config  = json.load(open('config.json'))
    # process command line options
    options = process_options(config)
    # initialize audio processor
    proc    = AudioProcessor(config, options)
    # setup command line
    cmd     = CommandLine(options, proc)

    # load and split
    proc.split_audiofile()

    if options.batch_mode:
        if not options.devide_mode:
            proc.push_all_chunks_loaded_to_new()
            proc.save_audiofile()
        else:
            proc.save_loaded_chunks_audiofiles()

        sys.exit(0)
    else:
        cmd.print_commands_help()
        cmd.repl()
